.globl _start;
_start: 
# Put your own code here
#x5 contains input n
#x28 , x29 tmp registers 
#x30 counts the number of terms
addi x5,x0,20
addi x28,x0,1
addi x29,x0,1
addi x30,x0,2
ble x5,x30,ex
loop:
add x6,x29,x28
addi x28,x29,0
addi x29,x6,0
addi x30,x30,1
bne x30,x5,loop
beq x30,x5,_exit
ex:
addi x6,x6,1;
_exit:
